FROM iron/go

WORKDIR /app
COPY ./main /app/

EXPOSE 8000
ENTRYPOINT ["./main"]
